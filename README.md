# Flatpak Environment

It's kinda like a venv, but flatpak not python

## Setup

```
$ fenv gen path-to/the.manifest.json
```

Will download and build deps setting up the environment in `.fenv`

## Use

```
$ fenv exec -- meson --prefix=/app _build
$ fenv exec -- ninja -C _build
$ fenv exec ./_build/your-app
```

That simple, fenv will search up the tree for a `.fenv` so you can move into subdirectories as you please

Of course a lot of commands need that `--` prefix, to avoid that (and save yourself even more typing) you can setup an alias

```
$ alias f="fenv exec --"
# Now you can just
$ f ninja -C _build
```