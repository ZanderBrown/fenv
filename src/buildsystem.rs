use std::{
    ffi::{OsStr, OsString},
    process::Command,
};

use serde_derive::Deserialize;

use crate::{context::Context, utils::ExtraArgs};

#[derive(Default, Deserialize)]
pub enum BuildSystem {
    #[default]
    #[serde(rename = "autotools")]
    Autotools,
    #[serde(rename = "cmake")]
    CMake,
    #[serde(rename = "cmake-ninja")]
    CMakeNinja,
    #[serde(rename = "meson")]
    Meson,
    #[serde(rename = "simple")]
    Simple,
    #[serde(rename = "qmake")]
    QMake,
}

impl ExtraArgs for BuildSystem {
    fn extra_args(&self, ctx: &Context) -> Vec<OsString> {
        match self {
            BuildSystem::CMake | BuildSystem::CMakeNinja | BuildSystem::Meson => {
                let mut build_fs_arg = OsString::from("--filesystem=");
                build_fs_arg.push(ctx.build_path());
                vec![build_fs_arg]
            }
            _ => Vec::new(),
        }
    }
}

impl BuildSystem {
    pub fn build_commands<I, S>(&self, ctx: &Context, args: I, rebuild: bool) -> Vec<Command>
    where
        I: IntoIterator<Item = S>,
        S: AsRef<OsStr>,
    {
        let module = ctx.manifest().module();

        let args: Vec<_> = args.into_iter().collect();

        match self {
            BuildSystem::Autotools => {
                let mut commands = Vec::new();

                if !rebuild {
                    let mut cmd = ctx.build_command(args.iter());
                    cmd.current_dir(ctx.root_path())
                        .args(["./configure", "--prefix=/app"])
                        .args(module.config_opts());
                    commands.push(cmd);
                }

                let mut cmd = ctx.build_command(args.iter());
                cmd.current_dir(ctx.root_path())
                    .args(["make", "-p", "-n", "-s"]);
                commands.push(cmd);

                let mut cmd = ctx.build_command(args);
                cmd.current_dir(ctx.root_path())
                    .args(["make", "V=0"])
                    .arg(format!("-j{}", num_cpus::get()))
                    .arg("install");
                commands.push(cmd);

                commands
            }
            BuildSystem::CMake | BuildSystem::CMakeNinja => {
                let mut commands = Vec::new();

                if !rebuild {
                    let mut cmd = ctx.build_command(args.iter());
                    cmd.args(["mkdir", "-p"]).arg(ctx.build_path());
                    commands.push(cmd);

                    let mut cmd = ctx.build_command(args.iter());
                    cmd.current_dir(ctx.build_path())
                        .args(["cmake", "-G", "Ninja", "..", "."])
                        .arg("-DCMAKE_EXPORT_COMPILE_COMMANDS=1")
                        .arg("-DCMAKE_BUILD_TYPE=RelWithDebInfo")
                        .arg("-DCMAKE_INSTALL_PREFIX=/app")
                        .args(module.config_opts());
                    commands.push(cmd);
                }

                let mut cmd = ctx.build_command(args.iter());
                cmd.current_dir(ctx.build_path()).arg("ninja");
                commands.push(cmd);

                let mut cmd = ctx.build_command(args);
                cmd.current_dir(ctx.build_path())
                    .arg("ninja")
                    .arg("install");
                commands.push(cmd);

                commands
            }
            BuildSystem::Meson => {
                let mut commands = Vec::new();

                if !rebuild {
                    let mut cmd = ctx.build_command(args.iter());
                    cmd.current_dir(ctx.root_path())
                        .args(["meson", "--prefix=/app"])
                        .arg(ctx.build_path())
                        .args(module.config_opts());
                    commands.push(cmd);
                }

                let mut cmd = ctx.build_command(args.iter());
                cmd.current_dir(ctx.root_path())
                    .args(["ninja", "-C"])
                    .arg(ctx.build_path());
                commands.push(cmd);

                let mut cmd = ctx.build_command(args);
                cmd.current_dir(ctx.root_path())
                    .args(["meson", "install", "-C"])
                    .arg(ctx.build_path());
                commands.push(cmd);

                commands
            }
            BuildSystem::Simple => module
                .simple_build_commands()
                .iter()
                .map(|step| {
                    let mut cmd = ctx.build_command(args.iter());
                    cmd.current_dir(ctx.root_path())
                        .args(["/bin/sh", "-c"])
                        .arg(step);
                    cmd
                })
                .collect(),
            BuildSystem::QMake => unimplemented!("Look, Bilal didn't do it either"),
        }
    }
}
