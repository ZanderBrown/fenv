use clap::{App, AppSettings, Arg, SubCommand};

mod buildsystem;
mod commands;
mod config;
mod context;
mod fontdirs;
mod manifest;
mod utils;

use commands::build::build_command;
use commands::bundle::bundle_command;
use commands::exec::exec_command;
use commands::gen::gen_command;
use commands::install::install_command;
use commands::run::run_command;
use commands::sdk_shell::sdk_shell_command;
use commands::shell::shell_command;

fn main() {
    let matches = App::new("Flatpak Environment")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Zander Brown <zbrown@gnome.org>")
        .about("Utility for flatpak based development")
        .setting(AppSettings::TrailingVarArg)
        .setting(AppSettings::ArgRequiredElseHelp)
        .subcommand(
            SubCommand::with_name("gen")
                .about("Run command in flatpak")
                .arg(
                    Arg::with_name("MANIFEST-PATH")
                        .required(true)
                        .help("Manifest to work on"),
                ),
        )
        .subcommand(
            SubCommand::with_name("exec")
                .about("Run command in flatpak")
                .arg(
                    Arg::with_name("COMMAND")
                        .multiple(true)
                        .allow_hyphen_values(true)
                        .help("Command to run"),
                ),
        )
        .subcommand(SubCommand::with_name("build").about("Build flatpak"))
        .subcommand(SubCommand::with_name("bundle").about("Create a flatpak bundle"))
        .subcommand(SubCommand::with_name("install").about("Install the flatpak"))
        .subcommand(SubCommand::with_name("run").about("Run flatpak"))
        .subcommand(
            SubCommand::with_name("shell").about("Launch a shell inside the build environment"),
        )
        .subcommand(SubCommand::with_name("sdk-shell").about("Enter SDK shell"))
        .get_matches();

    match matches.subcommand() {
        Some(("gen", matches)) => gen_command(matches),
        Some(("exec", matches)) => exec_command(matches),
        Some(("run", matches)) => run_command(matches),
        Some(("build", matches)) => build_command(matches),
        Some(("bundle", matches)) => bundle_command(matches),
        Some(("install", matches)) => install_command(matches),
        Some(("shell", matches)) => shell_command(matches),
        Some(("sdk-shell", matches)) => sdk_shell_command(matches),
        _ => {}
    }
}
