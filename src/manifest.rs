use serde_derive::Deserialize;
use users::get_current_uid;

use std::{
    collections::HashMap,
    ffi::{OsStr, OsString},
    path::PathBuf,
    process::{exit, Command},
};

use crate::{buildsystem::BuildSystem, context::Context, perror, utils::ExtraArgs};

#[derive(Deserialize)]
pub struct Manifest {
    #[serde(rename = "app-id", alias = "id")]
    app_id: String,
    #[allow(dead_code)]
    branch: Option<String>,
    modules: Vec<Module>,
    sdk: String,
    runtime: String,
    #[serde(rename = "runtime-version")]
    runtime_version: String,
    #[serde(rename = "sdk-extensions")]
    #[allow(dead_code)]
    sdk_extensions: Option<Vec<String>>,
    command: String,
    #[serde(rename = "finish-args")]
    finish_args: Vec<String>,
    #[serde(rename = "build-options")]
    build_options: Option<BuildOptions>,
}

#[derive(Deserialize)]
pub struct BuildOptions {
    #[serde(rename = "append-path")]
    append_path: Option<String>,
    #[serde(rename = "prepend-path")]
    prepend_path: Option<String>,
    #[serde(rename = "build-args")]
    #[serde(default)]
    build_args: Vec<String>,
    #[serde(default)]
    env: HashMap<String, String>,
}

impl ExtraArgs for Option<BuildOptions> {
    fn extra_args(&self, _ctx: &Context) -> Vec<OsString> {
        let mut res = Vec::new();
        if let Some(ref opts) = self {
            res.extend(opts.build_args.iter().map(OsString::from));

            let mut path_arg = OsString::from("--env=PATH=");
            if let Some(ref path) = opts.prepend_path {
                path_arg.push(path);
                path_arg.push(":");
            }
            path_arg.push(&OsString::from("/app/bin:/usr/bin"));
            if let Some(ref path) = opts.append_path {
                path_arg.push(":");
                path_arg.push(path);
            }
            res.push(path_arg);

            res.extend(
                opts.env
                    .iter()
                    .map(|(k, v)| OsString::from(format!("--env={}={}", k, v))),
            );
        }
        res
    }
}

#[derive(Deserialize)]
pub struct Module {
    name: String,
    #[serde(default)]
    buildsystem: BuildSystem,
    #[serde(rename = "config-opts")]
    #[serde(default)]
    config_opts: Vec<String>,
    #[allow(dead_code)]
    #[serde(default)]
    sources: Vec<Source>,
    #[serde(rename = "build-commands")]
    #[serde(default)]
    build_commands: Vec<String>,
}

impl Module {
    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn config_opts(&self) -> &[String] {
        &self.config_opts
    }

    pub fn simple_build_commands(&self) -> &[String] {
        &self.build_commands
    }
}

#[derive(Deserialize)]
pub struct Source {
    #[serde(rename = "type")]
    #[allow(dead_code)]
    type_: String,
    #[allow(dead_code)]
    url: Option<String>,
    #[allow(dead_code)]
    path: Option<String>,
    #[allow(dead_code)]
    tag: Option<String>,
    #[allow(dead_code)]
    commit: Option<String>,
    #[allow(dead_code)]
    sha256: Option<String>,
}

impl Manifest {
    pub fn load(path: PathBuf) -> Manifest {
        let mut cmd = crate::utils::flatpak_builder_cmd();
        let output = match cmd.arg("--show-manifest").arg(path).output() {
            Ok(output) => output,
            Err(err) => {
                perror!(
                    "Couldn't run the required dependency flatpak-builder: {}",
                    err
                );
                exit(1);
            }
        };

        if !output.status.success() {
            perror!("Failed to read manifest with flatpak-builder");
            println!("{}", std::str::from_utf8(&output.stderr).unwrap().trim());
            exit(1)
        }

        match serde_json::from_slice(&output.stdout) {
            Ok(res) => res,
            Err(err) => {
                perror!("Failed to read manifest");
                println!("{}", err);
                exit(1)
            }
        }
    }

    fn get_host_env() -> HashMap<String, String> {
        let forwarded_keys = [
            "COLORTERM",
            "DESKTOP_SESSION",
            "LANG",
            "WAYLAND_DISPLAY",
            "XDG_CURRENT_DESKTOP",
            "XDG_SEAT",
            "XDG_SESSION_DESKTOP",
            "XDG_SESSION_ID",
            "XDG_SESSION_TYPE",
            "XDG_VTNR",
            "AT_SPI_BUS_ADDRESS",
        ];

        let mut vars = HashMap::new();

        for (key, value) in std::env::vars() {
            if forwarded_keys.contains(&key.as_str()) {
                vars.insert(key, value);
            }
        }

        vars
    }

    pub fn exec<I, S>(&self, ctx: &Context, args: I, extensions: bool) -> Command
    where
        I: IntoIterator<Item = S>,
        S: AsRef<OsStr>,
    {
        let uid = get_current_uid();

        let mut cmd = Command::new("flatpak");
        cmd.arg("build")
            .arg("--with-appdir")
            .arg("--allow=devel")
            .arg(format!(
                "--bind-mount=/run/user/{}/doc=/run/user/{}/doc/by-app/{}",
                uid,
                uid,
                self.id()
            ))
            .args(self.finish_args().iter().filter(|arg| {
                !arg.starts_with("--metadata") && !arg.starts_with("--require-version")
            }))
            .arg("--talk-name=org.freedesktop.portal.*")
            .arg("--talk-name=org.a11y.Bus")
            .args(ctx.fontdirs().to_args())
            .args(
                Self::get_host_env()
                    .iter()
                    .map(|(k, v)| format!("--env={}={}", k, v)),
            );

        if extensions {
            cmd.args(self.build_options.extra_args(ctx))
                .arg("--share=network");
        }

        cmd.arg(ctx.repo_path()).args(args);

        cmd
    }

    pub fn id(&self) -> &String {
        &self.app_id
    }

    pub fn module(&self) -> &Module {
        if let Some(module) = self.modules.last() {
            module
        } else {
            perror!("No Nodules?");
            exit(1)
        }
    }

    pub fn finish_args(&self) -> &[String] {
        // _Might_ need to do something with --metadata here
        &self.finish_args
    }

    pub fn runtime_terminal(&self, _ctx: &Context) -> Command {
        let mut cmd = Command::new("flatpak");
        cmd.arg("run")
            .arg("--command=bash")
            .arg(format!("{}//{}", self.sdk, self.runtime_version));
        cmd
    }

    pub fn build_terminal(&self, ctx: &Context) -> Command {
        self.exec(ctx, vec![String::from("bash")], true)
    }

    pub fn init_build(&self, ctx: &Context) -> Command {
        let mut cmd = Command::new("flatpak");
        cmd.arg("build-init")
            .arg(ctx.repo_path())
            .arg(self.id())
            .arg(&self.sdk)
            .arg(&self.runtime)
            .arg(&self.runtime_version);
        cmd
    }

    pub fn update_dependencies(&self, ctx: &Context) -> Command {
        let mut state_dir_arg = OsString::from("--state-dir=");
        state_dir_arg.push(ctx.state_path());

        let mut cmd = crate::utils::flatpak_builder_cmd();
        cmd.args([
            "--ccache",
            "--force-clean",
            "--disable-updates",
            "--download-only",
        ])
        .arg(state_dir_arg)
        .arg(format!("--stop-at={}", self.module().name()))
        .arg(ctx.repo_path())
        .arg(ctx.manifest_path());

        cmd
    }

    pub fn build_dependencies(&self, ctx: &Context) -> Command {
        let mut state_dir_arg = OsString::from("--state-dir=");
        state_dir_arg.push(ctx.state_path());

        let mut cmd = crate::utils::flatpak_builder_cmd();
        cmd.args([
            "--ccache",
            "--force-clean",
            "--disable-updates",
            "--disable-download",
            "--build-only",
            "--keep-build-dirs",
        ])
        .arg(state_dir_arg)
        .arg(format!("--stop-at={}", self.module().name()))
        .arg(ctx.repo_path())
        .arg(ctx.manifest_path());

        cmd
    }

    pub fn build(&self, ctx: &Context, rebuild: bool) -> Vec<Command> {
        let mut args = vec![
            OsString::from("--share=network"),
            OsString::from("--nofilesystem=host"),
        ];

        let mut root_dir_arg = OsString::from("--filesystem=");
        root_dir_arg.push(ctx.root_path());
        args.push(root_dir_arg);

        let mut repo_dir_arg = OsString::from("--filesystem=");
        repo_dir_arg.push(ctx.repo_path());
        args.push(repo_dir_arg);

        args.extend(self.build_options.extra_args(ctx));

        let bs = &self.module().buildsystem;

        args.extend(bs.extra_args(ctx));

        bs.build_commands(ctx, args, rebuild)
    }

    pub fn build_finish(&self, ctx: &Context) -> Command {
        let mut cmd = Command::new("flatpak");
        cmd.arg("build-finish")
            .args(self.finish_args())
            .arg(&format!("--command={}", self.command))
            .arg(ctx.finalized_repo_path());
        cmd
    }

    pub fn build_export(&self, ctx: &Context) -> Command {
        let mut cmd = Command::new("flatpak");
        cmd.arg("build-export")
            .arg(ctx.ostree_repo_path())
            .arg(ctx.finalized_repo_path());
        cmd
    }

    pub fn bundle(&self, ctx: &Context) -> Vec<Command> {
        let mut cmds: Vec<Command> = Vec::new();

        cmds.push(self.build_finish(ctx));
        cmds.push(self.build_export(ctx));

        let mut bundle_cmd = Command::new("flatpak");
        bundle_cmd
            .arg("build-bundle")
            .arg(ctx.ostree_repo_path())
            .arg(format!("{}.flatpak", self.id()))
            .arg(self.id());
        cmds.push(bundle_cmd);

        cmds
    }

    pub fn install(&self, ctx: &Context) -> Vec<Command> {
        let mut cmds: Vec<Command> = Vec::new();

        cmds.push(self.build_finish(ctx));
        cmds.push(self.build_export(ctx));

        let mut install_cmd = Command::new("flatpak");
        install_cmd
            .arg("install")
            .arg(ctx.ostree_repo_path())
            .arg(self.id());
        cmds.push(install_cmd);

        cmds
    }

    pub fn run(&self, ctx: &Context) -> Command {
        self.exec(ctx, [&self.command], false)
    }
}
