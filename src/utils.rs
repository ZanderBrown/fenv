use std::{
    ffi::OsString,
    io::Error,
    os::unix::process::ExitStatusExt,
    process::{Command, Stdio},
};

use once_cell::sync::Lazy;

use crate::context::Context;

static IS_FLATPAK_BUILDER_SANDBOXED: Lazy<bool> = Lazy::new(|| {
    let mut cmd = Command::new("flatpak");
    cmd.stdout(Stdio::null());
    cmd.stderr(Stdio::null());
    cmd.args(["info", "org.flatpak.Builder"]);

    cmd.status()
        .map(|status| status.success() && status.code() == Some(0))
        .unwrap_or_default()
});

pub fn flatpak_builder_cmd() -> Command {
    if *IS_FLATPAK_BUILDER_SANDBOXED {
        let mut cmd = Command::new("flatpak");
        cmd.args(["run", "org.flatpak.Builder"]);

        cmd
    } else {
        Command::new("flatpak-builder")
    }
}

#[macro_export]
macro_rules! psuccess {
    ($($arg:tt)*) => ({
        use ansi_term::Colour::Green;
        print!("🎁 {}: ", Green.paint("SUCCESS"));
        println!($($arg)*);
    })
}

#[macro_export]
macro_rules! pinfo {
    ($($arg:tt)*) => ({
        use ansi_term::Colour::Blue;
        print!("🎁 {}: ", Blue.paint("INFO"));
        println!($($arg)*);
    })
}

#[macro_export]
macro_rules! pwarn {
    ($($arg:tt)*) => ({
        use ansi_term::Colour::Yellow;
        eprint!("🎁 {}: ", Yellow.paint("WARNING"));
        eprintln!($($arg)*);
    })
}

#[macro_export]
macro_rules! perror {
    ($($arg:tt)*) => ({
        use ansi_term::Colour::Red;
        eprint!("🎁 {}: ", Red.paint("ERROR"));
        eprintln!($($arg)*);
    })
}

pub enum Outcome {
    Success,
    Failed,
    Code(i32),
    Signal(i32),
    DidntStart(Error),
}

pub fn spawn_command(mut command: Command) -> Outcome {
    match command.spawn() {
        Ok(mut child) => match child.wait() {
            Ok(status) if status.success() => Outcome::Success,
            Ok(status) => {
                if let Some(code) = status.code() {
                    Outcome::Code(code)
                } else if let Some(num) = status.signal() {
                    Outcome::Signal(num)
                } else {
                    Outcome::Failed
                }
            }
            Err(err) => Outcome::DidntStart(err),
        },
        Err(err) => Outcome::DidntStart(err),
    }
}

pub trait ExtraArgs {
    fn extra_args(&self, ctx: &Context) -> Vec<OsString>;
}
