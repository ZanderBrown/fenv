use serde_derive::{Deserialize, Serialize};

use std::path::{Path, PathBuf};

#[derive(Deserialize, Serialize)]
pub struct Config {
    environment: Environment,
}

#[derive(Deserialize, Serialize)]
struct Environment {
    root: PathBuf,
    manifest: String,
}

impl Config {
    pub fn new(root: PathBuf, manifest: String) -> Self {
        Self {
            environment: Environment { root, manifest },
        }
    }
    pub fn root_path(&self) -> PathBuf {
        Path::new(&self.environment.root).to_path_buf()
    }

    pub fn manifest_path(&self) -> PathBuf {
        self.root_path().join(self.environment.manifest.clone())
    }
}
