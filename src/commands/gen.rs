use clap::ArgMatches;

use std::{env, process::exit};

use crate::{
    context::Context,
    perror, pinfo, psuccess,
    utils::{spawn_command, Outcome},
};

pub fn gen_command(matches: &ArgMatches) {
    let root = env::current_dir().expect("No working dir?");
    let manifest = matches.value_of("MANIFEST-PATH").expect("Erm, how?");

    pinfo!("Setting up “{}”", manifest);

    let ctx = Context::new(root, String::from(manifest));
    ctx.write();

    let manifest = ctx.manifest();

    match spawn_command(manifest.init_build(&ctx)) {
        Outcome::Success => (),
        Outcome::Code(code) => {
            perror!("Init Failed: Code {}", code);
            exit(code)
        }
        Outcome::Signal(num) => {
            perror!("Init Failed: Signal {}", num);
            exit(1)
        }
        Outcome::Failed => {
            perror!("Init Failed: No Idea Why");
            exit(1)
        }
        Outcome::DidntStart(err) => {
            perror!("Init Failed: Couldn’t Start Child: {}", err);
            exit(1)
        }
    }

    pinfo!("Fetching Dependencies");

    match spawn_command(manifest.update_dependencies(&ctx)) {
        Outcome::Success => (),
        Outcome::Code(code) => {
            perror!("Update Dependencies: Code {}", code);
            exit(code)
        }
        Outcome::Signal(num) => {
            perror!("Update Dependencies: Signal {}", num);
            exit(1)
        }
        Outcome::Failed => {
            perror!("Update Dependencies: No Idea Why");
            exit(1)
        }
        Outcome::DidntStart(err) => {
            perror!("Update Dependencies: Couldn’t Start Child: {}", err);
            exit(1)
        }
    }

    pinfo!("Building Dependencies");

    match spawn_command(manifest.build_dependencies(&ctx)) {
        Outcome::Success => (),
        Outcome::Code(code) => {
            perror!("Build Dependencies: Code {}", code);
            exit(code)
        }
        Outcome::Signal(num) => {
            perror!("Build Dependencies: Signal {}", num);
            exit(1)
        }
        Outcome::Failed => {
            perror!("Build Dependencies: No Idea Why");
            exit(1)
        }
        Outcome::DidntStart(err) => {
            perror!("Build Dependencies: Couldn’t Start Child: {}", err);
            exit(1)
        }
    }

    psuccess!("Environment Ready");
}
