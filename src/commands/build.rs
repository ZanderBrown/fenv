use std::process::exit;

use clap::ArgMatches;

use crate::{
    context::Context,
    perror, pinfo, psuccess,
    utils::{spawn_command, Outcome},
};

pub fn build_command(_matches: &ArgMatches) {
    let ctx = Context::load().expect("No config?");

    pinfo!("Working on “{}”", ctx.manifest().id());

    let commands = ctx.manifest().build(&ctx, false);

    for command in commands {
        match spawn_command(command) {
            Outcome::Success => (),
            Outcome::Code(code) => {
                perror!("Build Failed: Code {}", code);
                exit(code)
            }
            Outcome::Signal(num) => {
                perror!("Build Failed: Signal {}", num);
                exit(1)
            }
            Outcome::Failed => {
                perror!("Build Failed: No Idea Why");
                exit(1)
            }
            Outcome::DidntStart(err) => {
                perror!("Build Failed: Couldn’t Start Child: {}", err);
                exit(1)
            }
        }
    }

    psuccess!("Build Complete");
}
