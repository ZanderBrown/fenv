use std::process::exit;
use std::{fs::remove_dir_all, process::Command};

use clap::ArgMatches;

use crate::{
    context::Context,
    perror, pinfo, psuccess,
    utils::{spawn_command, Outcome},
};

pub fn install_command(_matches: &ArgMatches) {
    let ctx = Context::load().expect("No config?");

    pinfo!("Working on “{}”", ctx.manifest().id());

    // Remove the finalized build directory so we can re-run build-finalize
    if ctx.finalized_repo_path().exists() {
        remove_dir_all(ctx.finalized_repo_path())
            .expect("Could not clean up the finalized repo path.");
    }

    // Re-create the finalized build directory;
    // For some reason programmatic methods copy the folder
    // aren't working, so `cp` will have to do.
    let mut cp_cmd = Command::new("cp");
    cp_cmd
        .arg("-r")
        .arg(ctx.repo_path())
        .arg(ctx.finalized_repo_path());

    let mut commands = ctx.manifest().install(&ctx);
    commands.insert(0, cp_cmd);

    for command in commands {
        match spawn_command(command) {
            Outcome::Success => (),
            Outcome::Code(code) => {
                perror!("Installation Failed: Code {}", code);
                exit(code)
            }
            Outcome::Signal(num) => {
                perror!("Installation Failed: Signal {}", num);
                exit(1)
            }
            Outcome::Failed => {
                perror!("Installation Failed: No Idea Why");
                exit(1)
            }
            Outcome::DidntStart(err) => {
                perror!("Installation Failed: Couldn’t Start Child: {}", err);
                exit(1)
            }
        }
    }

    psuccess!("Bundling Complete");
}
