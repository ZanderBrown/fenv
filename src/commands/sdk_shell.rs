use std::process::exit;

use clap::ArgMatches;

use crate::{
    context::Context,
    perror,
    utils::{spawn_command, Outcome},
};

pub fn sdk_shell_command(_matches: &ArgMatches) {
    let ctx = Context::load().expect("No config?");

    let command = ctx.manifest().runtime_terminal(&ctx);

    match spawn_command(command) {
        Outcome::Success => (),
        Outcome::Code(code) => {
            perror!("Shell Failed: Code {}", code);
            exit(code)
        }
        Outcome::Signal(num) => {
            perror!("Shell Failed: Signal {}", num);
            exit(1)
        }
        Outcome::Failed => {
            perror!("Shell Failed: No Idea Why");
            exit(1)
        }
        Outcome::DidntStart(err) => {
            perror!("Shell Failed: Couldn’t Start Child: {}", err);
            exit(1)
        }
    }
}
