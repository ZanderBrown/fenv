use std::process::exit;

use clap::ArgMatches;

use crate::{
    context::Context,
    perror,
    utils::{spawn_command, Outcome},
};

pub fn run_command(_matches: &ArgMatches) {
    let ctx = Context::load().expect("No config?");

    let command = ctx.manifest().run(&ctx);

    match spawn_command(command) {
        Outcome::Success => (),
        Outcome::Code(code) => {
            perror!("Run Failed: Code {}", code);
            exit(code)
        }
        Outcome::Signal(num) => {
            perror!("Run Failed: Signal {}", num);
            exit(1)
        }
        Outcome::Failed => {
            perror!("Run Failed: No Idea Why");
            exit(1)
        }
        Outcome::DidntStart(err) => {
            perror!("Run Failed: Couldn’t Start Child: {}", err);
            exit(1)
        }
    }
}
