use std::process::exit;

use clap::ArgMatches;

use crate::{
    context::Context,
    perror, pinfo,
    utils::{spawn_command, Outcome},
};

pub fn shell_command(_matches: &ArgMatches) {
    let ctx = Context::load().expect("No config?");

    let command = ctx.manifest().build_terminal(&ctx);

    pinfo!("Starting shell");
    match spawn_command(command) {
        Outcome::Success => {
            pinfo!("Shell exited with success");
            exit(0)
        }
        Outcome::Code(code) => {
            perror!("Shell Failed: Code {}", code);
            exit(code)
        }
        Outcome::Signal(num) => {
            perror!("Shell Failed: Signal {}", num);
            exit(1)
        }
        Outcome::Failed => {
            perror!("Shell Failed: No Idea Why");
            exit(1)
        }
        Outcome::DidntStart(err) => {
            perror!("Shell Failed: Couldn’t Start Child: {}", err);
            exit(1)
        }
    }
}
