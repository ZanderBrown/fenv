use std::process::exit;

use clap::ArgMatches;

use crate::{
    context::Context,
    perror,
    utils::{spawn_command, Outcome},
};

pub fn exec_command(matches: &ArgMatches) {
    let command_to_run = matches.values_of("COMMAND").expect("do nothing?");

    let ctx = Context::load().expect("No config?");

    let command = ctx.manifest().exec(&ctx, command_to_run, true);

    match spawn_command(command) {
        Outcome::Success => (),
        Outcome::Code(code) => {
            perror!("Exec Failed: Code {}", code);
            exit(code)
        }
        Outcome::Signal(num) => {
            perror!("Exec Failed: Signal {}", num);
            exit(1)
        }
        Outcome::Failed => {
            perror!("Exec Failed: No Idea Why");
            exit(1)
        }
        Outcome::DidntStart(err) => {
            perror!("Exec Failed: Couldn’t Start Child: {}", err);
            exit(1)
        }
    }
}
