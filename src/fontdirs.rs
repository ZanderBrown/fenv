use once_cell::sync::Lazy;
use std::path::{Path, PathBuf};

static USR_SHARE_FONT: Lazy<PathBuf> = Lazy::new(|| PathBuf::from("/usr/share/fonts"));
static USR_LOCAL_SHARE_FONT: Lazy<PathBuf> = Lazy::new(|| PathBuf::from("/usr/share/local/fonts"));
static SYSTEM_FONT_CACHE_DIRS: Lazy<Vec<PathBuf>> = Lazy::new(|| {
    vec![
        PathBuf::from("/var/cache/fontconfig"),
        PathBuf::from("/usr/lib/fontconfig/cache"),
    ]
});

/// Font paths from the host which must be made available inside the fenv
///
/// `fenv` uses `flatpak build` to run a shell inside the build container. In contrast to `flatpak
/// run`, `flatpak build` does not make host fonts available inside the container. This means that
/// running applications from inside the `fenv` will look different to native applications. To get
/// around that we mount various font paths from the host inside the container.
///
/// This is effectively the same approach that gnome-builder takes. There is some discussion of the
/// issue in [1], the commit which added this functionality to gnome-builder can be found at [2],
/// and the implementation in `flatpak run` can be found at [3]
///
/// Note that in all the above implementations there is also some logic to generate a
/// `font-dirs.xml` file with `<remap>` elements which fontconfig understands  which is placed in
/// `/run/host/font-dirs.xml`. However, I (Alex Good) have not been able to find any logic which
/// actually tells fontconfig about this file in any of the common runtimes and not including this
/// file seems to make no difference, so we don't include it here.
///
/// A further note is that several of the files must be mounted using bind mounts, which makes them
/// read/write - this is probably undesirable but for the purposes of a development environment I
/// think it's okay.
///
/// [1]: https://github.com/flatpak/flatpak/issues/4404
/// [2]: https://github.com/GNOME/gnome-builder/commit/6e98bb98d72dadd34aec1665f4daebc989c56ce4
/// [3]: https://github.com/flatpak/flatpak/blob/0b4f1dadbfac91f6c884ac16e1465fb912ad6894/common/flatpak-run.c#L2209
pub(crate) struct FontDirs {
    usr_share: Option<PathBuf>,
    usr_local_share: Option<PathBuf>,
    cache_dir: Option<PathBuf>,
    user_datadir: Option<PathBuf>,
    user_cachedir: Option<PathBuf>,
}

impl FontDirs {
    /// Load the fontconfig paths from the host
    pub(crate) fn load() -> Result<Self, std::io::Error> {
        let usr_share = from_existing_dir(&*USR_SHARE_FONT);
        let usr_local_share = from_existing_dir(Path::new(&*USR_LOCAL_SHARE_FONT));
        let cache_dir = SYSTEM_FONT_CACHE_DIRS.iter().find_map(from_existing_dir);
        let basedirs = directories::BaseDirs::new();
        let data_dir = basedirs.as_ref().map(|d| d.data_dir());
        let user_datadir = data_dir
            .map(|d| from_existing_dir(d.join("fonts")))
            .or_else(|| {
                basedirs
                    .as_ref()
                    .map(|b| from_existing_dir(b.home_dir().join(".fonts")))
            })
            .flatten();

        let user_cachedir =
            basedirs.and_then(|d| from_existing_dir(d.cache_dir().join("fontconfig")));

        Ok(Self {
            usr_share,
            usr_local_share,
            cache_dir,
            user_datadir,
            user_cachedir,
        })
    }

    /// Return the arguments to add to `flatpak build` to mount the font directories
    pub(crate) fn to_args(&self) -> Vec<String> {
        let mut result = Vec::new();
        if let Some(usr_share) = &self.usr_share {
            result.push(format!(
                "--bind-mount=/run/host/fonts={}",
                usr_share.display()
            ));
        }

        if let Some(usr_local_share) = &self.usr_local_share {
            result.push(format!(
                "--bind-mount=/run/host/local-fonts={}",
                usr_local_share.display()
            ));
        }

        if let Some(cache_dir) = &self.cache_dir {
            result.push(format!(
                "--bind-mount=/run/host/fonts-cache={}",
                cache_dir.display()
            ));
        }

        if let Some(user_datadir) = &self.user_datadir {
            result.push(format!("--filesystem={}:ro", user_datadir.display()));
        }

        if let Some(user_cachedir) = &self.user_cachedir {
            result.push(format!("--filesystem={}:ro", user_cachedir.display()));
            result.push(format!(
                "--bind-mount=/run/host/user-fonts-cache={}",
                user_cachedir.display()
            ));
        }

        result
    }
}

fn from_existing_dir<P: AsRef<Path>>(path: P) -> Option<PathBuf> {
    let path = path.as_ref();
    if path.exists() && path.is_dir() {
        Some(path.to_path_buf())
    } else {
        None
    }
}
