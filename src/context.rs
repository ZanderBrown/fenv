use std::{env, ffi::OsStr, fs, path::PathBuf, process::Command};

use crate::{config::Config, fontdirs::FontDirs, manifest::Manifest, perror};

pub struct Context {
    config: Config,
    manifest: Manifest,
    fontdirs: FontDirs,
}

impl Context {
    const ENV_DIR: &'static str = ".fenv";
    const REPO_DIR: &'static str = "repo";
    const FINALIZED_REPO_DIR: &'static str = "repo-finalized";
    const OSTREE_REPO_DIR: &'static str = "ostree-repo";
    const STATE_DIR: &'static str = "state";
    const BUILD_DIR: &'static str = "_build";
    const CONFIG_NAME: &'static str = "config.toml";

    pub fn new(root: PathBuf, manifest_path: String) -> Self {
        fs::create_dir_all(root.join(Self::ENV_DIR)).expect("TODO: Handle errors");
        let manifest = Manifest::load(root.join(manifest_path.clone()));
        let fontdirs = FontDirs::load().unwrap();

        Self {
            config: Config::new(root, manifest_path),
            manifest,
            fontdirs,
        }
    }

    pub fn load() -> Result<Self, ()> {
        let current_dir = env::current_dir().expect("No working dir?");

        let mut look_in = current_dir;
        let config_file = loop {
            let maybe = look_in.join(Self::ENV_DIR).join(Self::CONFIG_NAME);
            if maybe.exists() {
                break Some(maybe);
            } else if let Some(parent) = look_in.parent() {
                look_in = parent.to_path_buf();
            } else {
                break None;
            }
        };

        if let Some(path) = config_file {
            let content = fs::read_to_string(path).expect("Yet can't read");
            let config: Config = toml::from_str(&content[..]).expect("Not toml?");
            let manifest = Manifest::load(config.manifest_path());
            let fontdirs = FontDirs::load().map_err(|_| ())?;
            Ok(Context {
                config,
                manifest,
                fontdirs,
            })
        } else {
            perror!("Can't find an environment");
            Err(())
        }
    }

    pub fn write(&self) {
        let string = toml::to_string(&self.config).expect("Wat, didn't encode");
        fs::write(
            self.config
                .root_path()
                .join(Self::ENV_DIR)
                .join(Self::CONFIG_NAME),
            string,
        )
        .expect("Huh, no write config");
    }

    pub fn root_path(&self) -> PathBuf {
        self.config.root_path()
    }

    pub fn repo_path(&self) -> PathBuf {
        self.config
            .root_path()
            .join(Self::ENV_DIR)
            .join(Self::REPO_DIR)
    }

    pub fn finalized_repo_path(&self) -> PathBuf {
        self.config
            .root_path()
            .join(Self::ENV_DIR)
            .join(Self::FINALIZED_REPO_DIR)
    }

    pub fn ostree_repo_path(&self) -> PathBuf {
        self.config
            .root_path()
            .join(Self::ENV_DIR)
            .join(Self::OSTREE_REPO_DIR)
    }

    pub fn state_path(&self) -> PathBuf {
        self.config
            .root_path()
            .join(Self::ENV_DIR)
            .join(Self::STATE_DIR)
    }

    pub fn build_path(&self) -> PathBuf {
        self.config.root_path().join(Self::BUILD_DIR)
    }

    pub fn manifest_path(&self) -> PathBuf {
        self.config.manifest_path()
    }

    pub(crate) fn fontdirs(&self) -> &FontDirs {
        &self.fontdirs
    }

    pub fn manifest(&self) -> &Manifest {
        &self.manifest
    }

    pub fn build_command<I, S>(&self, args: I) -> Command
    where
        I: IntoIterator<Item = S>,
        S: AsRef<OsStr>,
    {
        let mut cmd = Command::new("flatpak");
        cmd.arg("build").args(args).arg(self.repo_path());
        cmd
    }
}
